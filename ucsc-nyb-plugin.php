<?php
/*
Plugin Name: UCSC Normativas y Boletiness
Plugin URI: http://www.ucsc.cl
Description: Permite integrar normativas y boletines a la web, consumiendo y brindando servicios como softexpert, soap rest y api rest.
Version: 1.0.0
Author: Max Shtefec (Crombie Dev).
Author URI: http://www.yukei.net
License: propietary
*/

defined('ABSPATH') or die('Unauthorized Access');

// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation
 */
function activate_alecaddd_plugin() {
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_alecaddd_plugin' );

/**
 * The code that runs during plugin deactivation
 */
function deactivate_alecaddd_plugin() {
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_alecaddd_plugin' );

/**
 * Initialize all the core classes of the plugin
 */
if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}