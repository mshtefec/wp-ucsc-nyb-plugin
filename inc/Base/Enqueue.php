<?php

/**
 * @package UcscNybPlugin
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
	public function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}
	
	function enqueue() {
		// enqueue all our scripts
		wp_enqueue_style( 'ucsc-nyb-global-style', $this->plugin_url . 'assets/ucsc-nyb-global-style.css' );
		wp_enqueue_script( 'ucsc-nyb-global-script', $this->plugin_url . 'assets/ucsc-nyb-global-script.js' );
	}
}