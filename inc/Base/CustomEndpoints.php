<?php

/**
 * @package UcscNybPlugin
 */

namespace Inc\Base;

class CustomEndpoints
{
    public function register() {
		add_action( 'rest_api_init', array( $this, "custom_endpoints" ) );
	}

    function custom_endpoints() {
		
        register_rest_route('ucsc-nyb/api/v1', '/boletines', [
            'method' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_get')
        ]);
    
        register_rest_route('ucsc-nyb/api/v1', '/boletines/(?P<id>\d+)', [
            'method' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_get_one')
        ]);
        
        register_rest_route('ucsc-nyb/api/v1', '/boletines-normativas', [
            'method' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_normativas_get')
        ]);
        
        register_rest_route('ucsc-nyb/api/v1', '/boletines-normativas/(?P<id>\d+)', [
            'method' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_normativas_get_one')
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/boletines-normativas', [
            'method' => \WP_REST_SERVER::CREATABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_normativas_post'),
            'args' => array(),
            'permission_callback' => function () {
                return true;
            }
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/asociar-normativas-boletin', [
            'method' => \WP_REST_SERVER::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_search_and_post_boletin_normativas')
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/enviar-boletin', [
            'method' => \WP_REST_SERVER::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletin_new_document')
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/asociar-boletin', [
            'method' => \WP_REST_SERVER::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletin_new_assoc_document')
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/ejecutar-boletin', [
            'method' => \WP_REST_SERVER::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletin_execute_activity')
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/boletines', [
            'methods' => \WP_REST_Server::CREATABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletines_post'),
            'args' => array(),
            'permission_callback' => function () {
                return true;
            }
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/actualizar-boletin-estado', [
            'methods' => \WP_REST_Server::CREATABLE,
            'callback' => array($this, 'wp_ucsc_nyb_boletin_execute_status_post'),
            'args' => array(),
            'permission_callback' => function () {
                return true;
            }
        ]);

        register_rest_route('ucsc-nyb/api/v1', '/view-document-data/(?P<id>[-\w]+)', [
            'method' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'wp_ucsc_nyb_view_document_get_one')
        ]);

	}

    

}